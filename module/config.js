export const legends = {};

legends.stats = {
  "creativity": "legends.stats.creativity",
  "focus": "legends.stats.focus",
  "harmony": "legends.stats.harmony",
  "passion": "legends.stats.passion"
};

legends.approaches = {
  "defend-maneuver": "legends.techniques.approaches.defend-maneuver",
  "advance-attack": "legends.techniques.approaches.advance-attack",
  "evade-observe": "legends.techniques.approaches.evade-observe"
};

legends.training = {
  "water": "legends.training.water",
  "fire": "legends.training.fire",
  "earth": "legends.training.earth",
  "air": "legends.training.air",
  "weapons": "legends.training.weapons",
  "technology": "legends.training.technology"
}

legends.npcLevels = {
  "minor": "legends.actor-sheet.npc.levels.minor",
  "moderate": "legends.actor-sheet.npc.levels.moderate",
  "major": "legends.actor-sheet.npc.levels.major"
}