# Avatar Legends RPG (Unofficial)

Basic system for tracking Avatar Legends RPG characters

* https://magpiegames.com/avatarrpg

Screenshots available here: https://imgur.com/a/cA5Bfdg

## Installation
To install the system, either:

* Download the [project .zip](https://gitlab.com/pacosgrove1/legends/-/archive/main/legends-main.zip) and extract it to your systems installation directory, or
* Paste the manifest URL below into the Install System dialog on the Setup menu:
  * `https://gitlab.com/pacosgrove1/legends/-/raw/main/system.json`

## Usage
* Fatigue, Growth and NPC Principle tracks can be cleared by right-clicking.
* Context menus can be used to edit or delete Conditions, Features, Moves and Techniques added to a character sheet.

## Credits
This system exists thanks to the excellent YouTube tutorial series by [Cédric Hauteville](https://www.youtube.com/user/LieutenantRazak).

### Fonts
* [Bench Nine](https://www.fontsquirrel.com/fonts/benchnine) by Vernon Adams
* [Covered By Your Grace](https://fonts.google.com/specimen/Covered+By+Your+Grace?preview.text_type=alphabet&preview.size=37#standard-styles) by Kimberly Geswein
* [Hey August](https://www.behance.net/gallery/84310469/Free-Hey-August-Handwritten-Font) by Khurasan Studio
* [Komikazoom](https://www.1001fonts.com/komikazoom-font.html) by Apostrophic Labs
* [New Tegomin](https://fonts.google.com/specimen/New+Tegomin?preview.text_type=alphabet&preview.size=37&query=new+tegomin) by Kousuke Nagain
* [Village](https://www.dafont.com/prisoner.font) by Mark F. Heiman

_Licence details are included in the font folders._

### Images
* Icons from [Font Awesome](https://fontawesome.com/)
* Training images from [Game-icons.net](https://game-icons.net/)
